#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release
#![allow(unused)]
use serialport::SerialPort;
use std::env::args;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};

struct App {
    args: Args,
    port: Box<dyn SerialPort>,
    iobuffer: [u8; 32],
    command: [u8; 4],
}

struct Args {
    portname: String,
    channel: u8,
    interval: Duration,
}

impl App {
    fn new() -> Self {
        let args = parse_args();
        let port = serialport::new(args.portname.clone(), 9600)
            .data_bits(serialport::DataBits::Seven)
            .parity(serialport::Parity::Odd)
            .stop_bits(serialport::StopBits::One)
            .flow_control(serialport::FlowControl::None)
            .timeout(Duration::from_millis(1000))
            .open()
            .unwrap_or_else(|err| panic!("failed to open com-port {}: {}", args.portname, err));
        let iobuffer = [0_u8; 32];
        let mut command = [0_u8; 4];
        
        for (i, b) in format!("AV{}\r", args.channel).as_bytes().iter().enumerate() {
            command[i] = *b;
        }
        App {
            args,
            port,
            iobuffer,
            command,
        }
    }
    fn read_pressure(&mut self) -> Result<f64, std::io::Error> {
        self.clear_buf();
        if let Err(err) = self.port.write(&self.command) {
            eprintln!("failed to write to COM port: {}", err);
            return Err(err);
        };
        std::thread::sleep(Duration::from_millis(100));
        match self.port.read(&mut self.iobuffer) {
            Ok(bytes_read) => {
                // parse the result and log
                let p = parse_pressure(&self.iobuffer[..bytes_read]);
                Ok(p)
            }
            Err(err) => {
                eprintln!("failed to read from COM port: {}", err);
                Err(err)
            }
        }
    }
    fn clear_buf(&mut self) {
        for byte in self.iobuffer.iter_mut() {
            *byte = 0;
        }
    }
}

fn main() {
    let mut app = App::new();
    let should_close = Arc::new(AtomicBool::new(false));
    let should_close_ctrlc_handler = should_close.clone();
    ctrlc::set_handler(move || {
        should_close_ctrlc_handler.store(true, Ordering::Relaxed);
    })
    .expect("ERROR: Unable to set CTRL-C signal handler.");

    eprintln!(
        "Listening to MKS PR4000 pressure reading on {} (interval = {} s)",
        &app.args.portname,
        &app.args.interval.as_secs()
    );
    // first reading
    if let Ok(pressure) = app.read_pressure() {
        println!("[{}] reading channel {} = {}", get_time_string(), app.args.channel, pressure);
    }
    let mut tic = Instant::now();
    loop {
        if should_close.load(Ordering::Relaxed) {
            break;
        };

        std::thread::sleep(Duration::from_millis(100));

        if Instant::now() - tic > app.args.interval {
            tic = Instant::now();
            if let Ok(pressure) = app.read_pressure() {
                println!("[{}] reading channel {} = {}", get_time_string(), app.args.channel, pressure);
            }
        }
    }
    eprintln!("Exiting.")
}

fn parse_args() -> Args {
    let mut cli_args = args();
    let portname = match cli_args.nth(1) {
        Some(name) => name,
        None => usage(),
    };
    let channel = match cli_args.next().and_then(|s| s.parse::<u8>().ok()) {
        Some(ch) => if ch == 0 || ch > 2 {usage()} else {ch},
        None => usage(),
    };
    let interval = match cli_args.next().and_then(|s| s.parse::<u64>().ok()) {
        Some(secs) => Duration::from_secs(secs),
        None => usage(),
    };
    Args { portname, channel, interval }
}

fn get_time_string() -> String {
    chrono::Local::now().format("%Y-%m-%d %H:%M:%S").to_string()
}

fn usage() -> ! {
    let program_name = args()
        .next()
        .unwrap_or_else(|| panic!("unable to determine program name"));
    eprintln!(
        "Usage: {} <COM-PORT NAME> <CHANNEL (1 or 2)> <SECONDS BETWEEN READS>",
        program_name
    );
    std::process::exit(1);
}

fn parse_pressure(bytes: &[u8]) -> f64 {
    std::str::from_utf8(bytes)
        .ok()
        .and_then(|s| s.trim().parse::<f64>().ok())
        .unwrap_or(f64::NAN)
}
